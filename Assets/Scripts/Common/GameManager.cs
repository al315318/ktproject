﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [SerializeField] private Text nCubesText;
    [SerializeField] private Text totalCubesText;
    [SerializeField] private Text cubesDepletedText;
    [SerializeField] private Text pointerSizeText;
    [SerializeField] private Text timerText;
    [SerializeField] private Text endText;

    [SerializeField] private Button moreSizeButton;
    [SerializeField] private Button minusSizeButton;
    [SerializeField] private Button readyButton;
    [SerializeField] private Button resetButton;
    [SerializeField] private Button retryButton;
    [SerializeField] private Button exitButton;

    [SerializeField] private CubeManager2D cubeManager;
    [SerializeField] private GameObject pointer;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject cameraRig;
    [SerializeField] private GameObject cameraOrto;

    [SerializeField] private int maxPointerSize = 4;
    [SerializeField] private int minPointerSize = 1;
    [SerializeField] private int minutesTimer = 3;
    [SerializeField] private int secondsTimer = 30;

    private bool isReady = false;

    #region GETTERS && SETTERS

    public void SetIsReady(bool isReady)
    {
        this.isReady = isReady;
    }

    #endregion GETTERS && SETTERS

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if (nCubesText == null)
            throw new System.Exception($"{gameObject.name} necesita un nCubes");
        if (totalCubesText == null)
            throw new System.Exception($"{gameObject.name} necesita un nNumberCubes");
        if (cubesDepletedText == null)
            throw new System.Exception($"{gameObject.name} necesita un cubesDepleted");
        if (cubeManager == null)
            throw new System.Exception($"{gameObject.name} necesita un cubeManager");
        if (pointer == null)
            throw new System.Exception($"{gameObject.name} necesita un pointer");
        if (moreSizeButton == null)
            throw new System.Exception($"{gameObject.name} necesita un moreSizeButton");
        if (minusSizeButton == null)
            throw new System.Exception($"{gameObject.name} necesita un minusSizeButton");
        if (pointerSizeText == null)
            throw new System.Exception($"{gameObject.name} necesita un pointerSize");
        if (readyButton == null)
            throw new System.Exception($"{gameObject.name} necesita un readyButton");
        if (resetButton == null)
            throw new System.Exception($"{gameObject.name} necesita un resetButton");
        if (timerText == null)
            throw new System.Exception($"{gameObject.name} necesita un timerText");
        if (player == null)
            throw new System.Exception($"{gameObject.name} necesita un player");
        if (cameraRig == null)
            throw new System.Exception($"{gameObject.name} necesita un cameraRig");
        if (cameraOrto == null)
            throw new System.Exception($"{gameObject.name} necesita un cameraOrto");
        if (retryButton == null)
            throw new System.Exception($"{gameObject.name} necesita un retryButton");
        if (endText == null)
            throw new System.Exception($"{gameObject.name} necesita un endText");
        if (exitButton == null)
            throw new System.Exception($"{gameObject.name} necesita un exitButton");

        InitDesignStage(true);
    }

    public void InitDesignStage(bool changePos)
    {
        isReady = false;
        nCubesText.gameObject.SetActive(true);
        nCubesText.text = $"1/{cubeManager.GetMaxCubes()}";
        cubesDepletedText.gameObject.SetActive(false);
        totalCubesText.gameObject.SetActive(true);

        minusSizeButton.gameObject.SetActive(false);
        moreSizeButton.gameObject.SetActive(true);
        readyButton.gameObject.SetActive(false);
        resetButton.gameObject.SetActive(true);

        pointerSizeText.gameObject.SetActive(true);
        pointer.transform.localScale = new Vector3(1, 1, 1);
        pointerSizeText.text = $"{pointer.transform.localScale.x}";
        timerText.gameObject.SetActive(false);
        pointer.SetActive(true);
        cameraRig.SetActive(false);
        cameraOrto.SetActive(true);
        player.SetActive(false);
        StopAllCoroutines();
        cubeManager.enabled = true;
        cubeManager.ResetCubes();
        if (changePos)
        {
            cubeManager.GetEndPoint().transform.position = new Vector3(cubeManager.GetEndPoint().transform.position.x, cubeManager.GetEndPoint().transform.position.y + 5, cubeManager.GetEndPoint().transform.position.z);
        }
        endText.gameObject.SetActive(false);
        retryButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void SetNCubes(int nCubes)
    {
        if (nCubes > cubeManager.GetMaxCubes())
        {
            cubesDepletedText.gameObject.SetActive(true);
            readyButton.gameObject.SetActive(false);
        }
        else
        {
            cubesDepletedText.gameObject.SetActive(false);
            if (isReady)
            {
                readyButton.gameObject.SetActive(true);
            }
        }
        nCubesText.text = $"{nCubes}/{cubeManager.GetMaxCubes()}";
    }

    public void MoreSize()
    {
        minusSizeButton.gameObject.SetActive(true);
        if (pointer.transform.localScale.x < maxPointerSize)
        {
            pointer.transform.localScale = new Vector3(pointer.transform.localScale.x + 1, pointer.transform.localScale.y + 1, pointer.transform.localScale.z + 1);
            if (pointer.transform.localScale.x == maxPointerSize)
            {
                moreSizeButton.gameObject.SetActive(false);
            }
        }
        pointerSizeText.text = $"{pointer.transform.localScale.x}";
    }

    public void MinusSize()
    {
        moreSizeButton.gameObject.SetActive(true);
        if (pointer.transform.localScale.x > minPointerSize)
        {
            pointer.transform.localScale = new Vector3(pointer.transform.localScale.x - 1, pointer.transform.localScale.y - 1, pointer.transform.localScale.z - 1);
            if (pointer.transform.localScale.x == minPointerSize)
            {
                minusSizeButton.gameObject.SetActive(false);
            }
        }
        pointerSizeText.text = $"{pointer.transform.localScale.x}";
    }

    public void ResetCubes()
    {
        cubeManager.ResetCubes();
        InitDesignStage(false);
    }

    public void Ready()
    {
        nCubesText.gameObject.SetActive(false);
        cubesDepletedText.gameObject.SetActive(false);
        totalCubesText.gameObject.SetActive(false);

        minusSizeButton.gameObject.SetActive(false);
        moreSizeButton.gameObject.SetActive(false);
        readyButton.gameObject.SetActive(false);
        resetButton.gameObject.SetActive(false);

        pointerSizeText.gameObject.SetActive(false);
        timerText.gameObject.SetActive(true);
        timerText.text = $"{minutesTimer}:{secondsTimer}";
        cubeManager.enabled = false;
        pointer.SetActive(false);

        cameraOrto.SetActive(false);
        cameraRig.SetActive(true);
        player.transform.position = new Vector3(0, player.transform.position.y, 0);
        player.SetActive(true);
        StartCoroutine(StartCountdown(minutesTimer * 60 + secondsTimer));
        cubeManager.GetEndPoint().transform.position = new Vector3(cubeManager.GetEndPoint().transform.position.x, cubeManager.GetEndPoint().transform.position.y - 5, cubeManager.GetEndPoint().transform.position.z);
        endText.gameObject.SetActive(false);
        retryButton.gameObject.SetActive(false);
        exitButton.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void Lose()
    {
        endText.gameObject.SetActive(true);
        retryButton.gameObject.SetActive(true);
        exitButton.gameObject.SetActive(true);
        endText.text = $"Defeated!!\n\nToo late, buuuuh";
        StopAllCoroutines();
        Time.timeScale = 0;
    }

    public void Win()
    {
        endText.gameObject.SetActive(true);
        retryButton.gameObject.SetActive(true);
        exitButton.gameObject.SetActive(true);
        endText.text = $"You Win!!\n\nYou're better than your comrade";
        StopAllCoroutines();
        Time.timeScale = 0;
    }

    public void Exit()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public IEnumerator StartCountdown(float countdownValue)
    {
        float currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            timerText.text = $"{(int)currCountdownValue / 60}:{currCountdownValue % 60}";
            if (currCountdownValue.Equals(1))
            {
                Lose();
                yield return null;
            }
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
    }
}