﻿using System.Collections.Generic;
using UnityEngine;

public class CubeManager2D : MonoBehaviour
{
    //[SerializeField] private GameObject cube;
    [SerializeField] private List<GameObject> cubes;

    [SerializeField] private GameObject pointer;
    [SerializeField] private GameObject endPoint;
    [SerializeField] private int maxY = 0;
    [SerializeField] private int maxX = 0;
    [SerializeField] private int maxCubes = 400;
    [SerializeField] private int centerSecureZoneRadious = 5;
    [SerializeField] private int cubesTotalNumber = 1;

    [SerializeField] private List<GameObject> spawnerObjects = new List<GameObject>();

    #region GETTERS && SETTERS

    public int GetMaxX()
    {
        return maxX;
    }

    public int GetMaxY()
    {
        return maxY;
    }

    public int GetMaxCubes()
    {
        return maxCubes;
    }

    public int GetCubesTotalNumber()
    {
        return this.cubesTotalNumber;
    }

    public GameObject GetEndPoint()
    {
        return endPoint;
    }

    #endregion GETTERS && SETTERS

    private Dictionary<int, HashSet<int>> visitedCubes = new Dictionary<int, HashSet<int>>();
    private Dictionary<int, HashSet<int>> activeCubes = new Dictionary<int, HashSet<int>>();
    private int finalPosX;
    private int finalPosY;

    private void Awake()
    {
        //if (cube == null)
        //   throw new System.Exception($"{gameObject.name} necesita un cubo que instanciar");
        if (cubes == null)
            throw new System.Exception($"{gameObject.name} necesita los cubos ya posicionados");
        if (pointer == null)
            throw new System.Exception($"{gameObject.name} necesita un pointer");
        if (endPoint == null)
            throw new System.Exception($"{gameObject.name} necesita un endPoint");
        if (spawnerObjects != null && spawnerObjects.Count == 0)
            throw new System.Exception($"{gameObject.name} necesita un spawnerObjects");
    }

    private void Start()
    {
        pointer.SetActive(false);
        SetEndPosition();
    }

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        pointer.transform.position = new Vector3(ray.GetPoint(Camera.main.transform.position.y).x, 0, ray.GetPoint(Camera.main.transform.position.y).z);

        if (Input.GetMouseButton(0))
        {
            pointer.SetActive(true);
        }
        else
        {
            pointer.SetActive(false);
        }
        Debug.DrawLine(ray.origin, ray.direction);
    }

    private void SetEndPosition()
    {
        finalPosX = Mathf.RoundToInt(Random.Range(0, 2)) == 1 ? -Random.Range(centerSecureZoneRadious, maxX) : Random.Range(centerSecureZoneRadious, maxX);
        finalPosY = Mathf.RoundToInt(Random.Range(0, 2)) == 1 ? -Random.Range(centerSecureZoneRadious, maxY) : Random.Range(centerSecureZoneRadious, maxY);

        endPoint.transform.position = new Vector3(finalPosX, endPoint.transform.position.y, finalPosY);
    }

    //Añadimos un cubo en las perpendiculares solo si no los hemos visitado ya
    private void AddActiveCube(int key, int value)
    {
        HashSet<int> visitedY = new HashSet<int>();
        if (visitedCubes.TryGetValue(key, out visitedY))
        {
            if (visitedY.Contains(value))
            {
                return;
            }
        }
        HashSet<int> l = new HashSet<int>();

        if (activeCubes.TryGetValue(key, out l))
        {
            if (l.Contains(value))
            {
                return;
            }

            l.Add(value);
            activeCubes[key] = l;
        }
        else
        {
            activeCubes[key] = new HashSet<int>() { value };
        }

        int i = Random.Range(0, spawnerObjects.Count);

        GameObject instCube = Instantiate(spawnerObjects[i], new Vector3(key, 0, value), Quaternion.identity, transform);
        instCube.SetActive(true);
        Cube c = instCube.GetComponent<Cube>();
        c.SetCubeManager(this);
        c.SetX(key);
        c.SetY(value);
        cubesTotalNumber++;
        GameManager.instance.SetNCubes(cubesTotalNumber);
    }

    public void RemoveCube(int key, int value)
    {
        if (activeCubes.ContainsKey(key))
        {
            HashSet<int> l = activeCubes[key];
            l.Remove(value);
            activeCubes[key] = l;
        }

        if (visitedCubes.ContainsKey(key))
        {
            HashSet<int> l = visitedCubes[key];
            l.Add(value);
            visitedCubes[key] = l;
        }
        else
        {
            visitedCubes.Add(key, new HashSet<int>() { value });
        }

        if (key == finalPosX && value == finalPosY)
        {
            GameManager.instance.SetIsReady(true);
        }

        AddActiveCube(key + 1, value);
        AddActiveCube(key - 1, value);
        AddActiveCube(key, value + 1);
        AddActiveCube(key, value - 1);
        cubesTotalNumber--;
        GameManager.instance.SetNCubes(cubesTotalNumber);
    }

    public void ResetCubes()
    {
        foreach (Transform child in this.transform)
        {
            Destroy(child.gameObject);
        }

        activeCubes.Clear();
        visitedCubes.Clear();
        GameObject instCube = Instantiate(spawnerObjects[0], transform);
        instCube.SetActive(true);
        Cube c = instCube.GetComponent<Cube>();
        c.SetCubeManager(this);
        c.SetX(0);
        c.SetY(0);
        cubesTotalNumber = 1;
        GameManager.instance.SetNCubes(cubesTotalNumber);
    }

    private void OnEnable()
    {
        SetEndPosition();
    }
}