﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    [SerializeField] private int x;
    [SerializeField] private int y;
    [SerializeField] private CubeManager2D cubeManager2D;

    #region GETTERS & SETTERS

    public void SetX(int x)
    {
        this.x = x;
    }

    public void SetY(int y)
    {
        this.y = y;
    }

    public void SetCubeManager(CubeManager2D cubeManager2D)
    {
        this.cubeManager2D = cubeManager2D;
    }

    #endregion GETTERS & SETTERS

    private void Start()
    {
        if (cubeManager2D == null)
            throw new System.Exception($"{gameObject.name} necesita un cubeManager2D");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (x == cubeManager2D.GetMaxX() || x == -cubeManager2D.GetMaxX() || y == cubeManager2D.GetMaxY() || y == -cubeManager2D.GetMaxY())
        {
            return;
        }

        if (other.tag == "CubeDestroyer")
        {
            if (cubeManager2D != null)
            {
                cubeManager2D.RemoveCube(x, y);
            }

            Destroy(gameObject);
        }
    }
}