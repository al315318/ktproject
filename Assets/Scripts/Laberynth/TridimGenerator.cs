﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TridimGenerator : MonoBehaviour
{
    [SerializeField]
    [Range(1, 999)]
    private float width = 4;

    [SerializeField]
    [Range(1, 999)]
    private float height = 4;

    [SerializeField]
    [Range(1, 999)]
    private float depth = 4;

    [SerializeField]
    [Range(1, 1000)]
    private int resolutionWidth = 10;

    [SerializeField]
    [Range(1, 1000)]
    private int resolutionHeight = 10;

    [SerializeField]
    [Range(1, 1000)]
    private int resolutionDepth = 10;

    [SerializeField]
    private GameObject point;

    private void Start()
    {
        if (point == null)
            throw new System.Exception(gameObject.name + " necesita un objeto que instanciar");

        GeneratePoints();
    }

    private void GeneratePoints()
    {
        Vector3 start = new Vector3(transform.position.x - (width / 2), transform.position.y - (height / 2), transform.position.z - (depth / 2));

        for (int x = 0; x < resolutionWidth; x++)
        {
            for (int y = 0; y < resolutionHeight; y++)
            {
                for (int z = 0; z < resolutionDepth; z++)
                {
                    //Encontrar un nuevo punto para el objeto

                    float pointX = start.x;
                    float pointY = start.y;
                    float pointZ = start.z;

                    if (resolutionWidth > 1)
                        pointX += (x * width / (resolutionWidth - 1));
                    if (resolutionHeight > 1)
                        pointY += (y * height / (resolutionHeight - 1));
                    if (resolutionDepth > 1)
                        pointZ += (z * depth / (resolutionDepth - 1));

                    Vector3 nextPosition = new Vector3(pointX, pointY, pointZ);
                    //Rotar un punto sobre un pivote
                    nextPosition = transform.rotation * (nextPosition - transform.position) + transform.position;
                    //Actualmente se usa la posicion original del objeto como suma a la posicion calculada.
                    //Se usa la rotacion original del objeto
                    GameObject instantiatedObject = Instantiate(point, nextPosition + point.transform.position, point.transform.rotation, transform);
                    instantiatedObject.SetActive(false);
                }
            }
        }
    }
}