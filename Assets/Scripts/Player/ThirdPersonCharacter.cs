using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class ThirdPersonCharacter : MonoBehaviour
    {
        [SerializeField] private float m_MovingTurnSpeed = 360;
        [SerializeField] private float m_StationaryTurnSpeed = 180;
        [SerializeField] private float m_MoveSpeedMultiplier = 1f;

        private Rigidbody m_Rigidbody;
        private const float k_Half = 0.5f;
        private float m_TurnAmount;
        private float m_ForwardAmount;
        private Vector3 m_GroundNormal;

        private void Start()
        {
            m_Rigidbody = GetComponent<Rigidbody>();

            m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }

        public void Move(Vector3 move)
        {
            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.
            Vector3 moveLocal = move;
            if (moveLocal.magnitude > 1f) moveLocal.Normalize();
            moveLocal = transform.InverseTransformDirection(moveLocal);
            moveLocal = Vector3.ProjectOnPlane(moveLocal, m_GroundNormal);
            m_TurnAmount = Mathf.Atan2(moveLocal.x, moveLocal.z);
            m_ForwardAmount = moveLocal.z;

            ApplyExtraTurnRotation();

            m_Rigidbody.MovePosition(transform.position + (move * m_MoveSpeedMultiplier));
        }

        private void ApplyExtraTurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
            transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.tag == "EndPoint")
            {
                GameManager.instance.Win();
            }
        }
    }
}